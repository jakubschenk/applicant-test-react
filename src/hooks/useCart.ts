import React, { useContext, useState } from 'react';
import { CartItemT, context } from '../context/CartContext';
import { Item } from '../item';

export const useCartHook = () => {
  const [items, setItems] = useState<CartItemT[]>([]);

  const removeItem = (item: Item) => {
    setItems(items.filter((i) => i.id !== item.id));
    return items;
  };

  const addItem = (item: Item, count: number) => {
    const filteredItems = removeItem(item);
    setItems([
      ...filteredItems,
      {
        ...item,
        count: count,
      },
    ]);
  };

  const clear = () => {
    setItems([]);
  };

  return {
    items,
    removeItem,
    addItem,
    clear,
  };
};

const useCart = () => useContext(context);

export default useCart;
