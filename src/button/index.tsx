import React, {
  forwardRef,
  PropsWithChildren,
  ButtonHTMLAttributes,
} from 'react';
import classNames from 'classnames';

export type ButtonProps = PropsWithChildren<
  ButtonHTMLAttributes<HTMLButtonElement>
>;

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ children, className, ...props }, ref) => (
    <button
      ref={ref}
      role="button"
      type="button"
      className={classNames(
        'border-2 border-blue-200 rounded-md p-1 m-2',
        className
      )}
      {...props}
    >
      {children}
    </button>
  )
);

export default Button;
