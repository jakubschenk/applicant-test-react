import React, { FunctionComponent } from 'react';
import { Link } from 'react-router-dom';
import CartImage from '../static/cart.png';

export type NavbarProps = {
  title?: string;
};

const Navbar: FunctionComponent<NavbarProps> = ({ title }) => {
  return (
    <nav className="navbar">
      <div className="navbar-item books-btn">
        <Link to="/">Books</Link>
      </div>
      <div className="navbar-item">
        <Link to="/">{title}</Link>
      </div>
      <div className="navbar-item cart-btn">
        <Link to="cart">
          <img src={CartImage} alt="Cart Button" />
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;
