import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Products from '../products';
import Cart from '../cart';

const Router = () => (
  <Routes>
    <Route path="/" element={<Products />} />
    <Route path="cart" element={<Cart />} />
  </Routes>
);

export default Router;
