import React from 'react';
import Router from './router';
import Layout from './layout';
import { CartProvider } from './context/CartContext';

const App = () => (
  <CartProvider>
    <Layout>
      <Router />
    </Layout>
  </CartProvider>
);

export default App;
