import React, { ChangeEvent, FunctionComponent, useState } from 'react';
import useCart from '../hooks/useCart';
import Button from '../button';

export type Item = {
  id: number;
  name: string;
  description: string;
  img: string;
  price: number;
  stock: number;
  type?: 'book' | 'other';
};

export type ItemProps = {
  item: Item;
};

export const ItemsWrapper: FunctionComponent = ({ children }) => (
  <div className="book-list">{children}</div>
);

const ItemCard: FunctionComponent<ItemProps> = ({ item }) => {
  const { addItem } = useCart();
  const [addToCartCount, setAddToCartCount] = useState<string>('0');

  const handleInput = (event: ChangeEvent<HTMLInputElement>) => {
    setAddToCartCount(event.currentTarget.value);
  };

  const handleItemAdd = () => {
    const count = parseInt(addToCartCount, 10);
    if (count > 0 && count <= item.stock) {
      // TODO: Show a toast or an alert (toast preferably)
      addItem(item, count);
    }
  };

  return (
    <div className="book-card">
      <div className="book-img">
        <img src={item.img} alt={item.name} />
      </div>
      <span className="book-name">{item.name}</span>
      <span className="book-description">{item.description}</span>
      <div className="flex flex-row items-center justify-around w-full">
        <div className="book-price">
          {item.price} CZK ({item.stock > 6 ? item.stock : '5+'} in stock)
        </div>
        {item.stock === 0 ? (
          <div className="book-unavailable">Not available</div>
        ) : (
          <div className="book-add-section space-x-2">
            <input
              type="text"
              pattern="(^[0-9]+$|^$|^\s$)"
              onChange={handleInput}
              defaultValue={0}
              value={addToCartCount}
              className="w-10 bg-slate-200 border border-gray-500 rounded-md p-1 m-2"
            />
            <Button onClick={handleItemAdd}>Add to cart</Button>
          </div>
        )}
      </div>
    </div>
  );
};

export default ItemCard;
