import React from 'react';
import ItemCard, { ItemsWrapper, Item } from '../item';
// This would get fetched normally
import content from '../static/content.json';

const Products = () => {
  return (
    <ItemsWrapper>
      {content.books.map((item: Item) => (
        <ItemCard item={item} key={item.name + item.id} />
      ))}
    </ItemsWrapper>
  );
};

export default Products;
