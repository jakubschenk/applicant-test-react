import React, { FunctionComponent } from 'react';
import { CartItemT } from '../context/CartContext';
import Button from '../button';

export type CartItemProps = {
  item: CartItemT;
  onDelete: (item: CartItemT) => CartItemT[];
};

const CartItem: FunctionComponent<CartItemProps> = ({ item, onDelete }) => {
  const handleDelete = () => {
    onDelete(item);
  };

  return (
    <tr className="book-row flex items-center">
      <td className="book-name">{item.name}</td>
      <td className="amount">
        {item.count} <Button onClick={handleDelete}>Remove</Button>
      </td>
      <td className="price">{item.price * item.count} CZK</td>
    </tr>
  );
};

export default CartItem;
