import React, {
  Context,
  createContext,
  FunctionComponent,
  PropsWithChildren,
  ReactNode,
  useContext,
} from 'react';
import { Item } from '../item';
import { useCartHook } from '../hooks/useCart';

export type CartItemT = {
  count: number;
} & Item;

export type CartContextProps = {
  items: CartItemT[];
  removeItem: (item: Item) => CartItemT[];
  addItem: (item: Item, count: number) => void;
  clear: () => void;
};

export type CartProviderProps = {
  children: ReactNode;
};

export const context = createContext<CartContextProps>({} as CartContextProps);

export const CartProvider: FunctionComponent<CartProviderProps> = ({
  children,
}) => {
  const cart = useCartHook();
  return <context.Provider value={cart}>{children}</context.Provider>;
};
