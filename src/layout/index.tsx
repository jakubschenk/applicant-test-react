import React, { FunctionComponent } from 'react';
import Navbar from '../navbar';

const Layout: FunctionComponent = ({ children }) => (
  <>
    <Navbar title="Awesome book store" />
    <div className="main-area">{children}</div>
  </>
);

export default Layout;
