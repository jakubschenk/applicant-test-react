import React from 'react';
import CartItem from '../cart-item';
import useCart from '../hooks/useCart';

const Cart = () => {
  const { items, removeItem } = useCart();
  return (
    <table className="cart-list">
      <tr className="main-row">
        <th className="name">Name</th>
        <th className="amount">Amount</th>
        <th className="price">Price</th>
      </tr>
      {items.map((item) => (
        <CartItem item={item} onDelete={removeItem} key={item.id + item.name} />
      ))}
      <tr className="main-row">
        <th className="name">Total</th>
        <th></th>
        <th className="price">
          {items.reduce((sum, current) => sum + current.price, 0)} CZK
        </th>
      </tr>
    </table>
  );
};

export default Cart;
