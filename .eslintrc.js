module.exports = {
  parser: '@typescript-eslint/parser',
  env: {
    jest: true,
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    // Make sure to put `prettier` last, so it gets the chance to override other configs.
    'prettier',
  ],
  rules: {
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-non-null-assertion': 'off',
    curly: 'error',
    // We don't want eslint errors for unsused vars, TS compiler already takes care of that. See tsconfig.json.
    '@typescript-eslint/no-unused-vars': 'off',
    'import/order': 'warn',
    'jest/no-mocks-import': 'off',
    'no-console': 'error',
    'react/display-name': 'off',
    // We will use TypeScript's types for component props instead.
    'react/prop-types': 'off',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  globals: {},
  overrides: [
    // This configuration will apply only to test files
    {
      files: ['**/*.test.ts', '**/*.test.tsx'],
      extends: ['plugin:jest/recommended', 'plugin:jest/style'],
    },
  ],
};
