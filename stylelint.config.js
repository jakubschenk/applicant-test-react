module.exports = {
  extends: [
    "stylelint-config-standard",
    "stylelint-config-recommended",
    "stylelint-config-prettier",
  ],
  plugins: ["stylelint-order"],
  rules: {
    "at-rule-no-unknown": [
      true,
      {
        ignoreAtRules: [
          "apply",
          "components",
          "extends",
          "layer",
          "screen",
          "tailwind",
          "utilities",
          "value",
        ],
      },
    ],
    "order/order": ["custom-properties", "declarations"],
    "order/properties-order": [
      "display",
      "flex",
      "flex-direction",
      "align-items",
      "justify-content",
      "position",
      "width",
      "height",
      "padding",
      "margin",
    ],
  },
};
